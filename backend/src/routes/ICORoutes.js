const express = require('express');
const router = express.Router();
const ICOController = require('@controllers/ICOController');
const { authenticateJWT } = require('@middleware/authMiddleware');

router.post('/topic', authenticateJWT, ICOController.createICO);
router.post('/topic/invest', authenticateJWT, ICOController.investInICO);
router.get('/topic/:icoId', ICOController.fetchICODetails);

module.exports = router;