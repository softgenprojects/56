const ICOServices = require('@services/ICOServices');

const createICO = async (req, res) => {
  const { name, description, startTime, endTime, tokenName, tokenSymbol } = req.body;
  try {
    const ico = await ICOServices.createICO({ name, description, startTime, endTime, tokenName, tokenSymbol });
    res.status(201).json(ico);
  } catch (error) {
    res.status(500).json({ message: 'Error creating ICO', error: error.message });
  }
};

const investInICO = async (req, res) => {
  const { userId, icoId, amount } = req.body;
  try {
    const investmentResult = await ICOServices.investInICO({ userId, icoId, amount });
    res.status(200).json({ message: investmentResult });
  } catch (error) {
    res.status(500).json({ message: 'Error investing in ICO', error: error.message });
  }
};

const fetchICODetails = async (req, res) => {
  const { icoId } = req.params;
  try {
    const icoDetails = await ICOServices.fetchICODetails(icoId);
    res.status(200).json(icoDetails);
  } catch (error) {
    res.status(500).json({ message: 'Error fetching ICO details', error: error.message });
  }
};

module.exports = { createICO, investInICO, fetchICODetails };