const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createICO = async ({ name, description, startTime, endTime, tokenName, tokenSymbol }) => {
  return await prisma.iCO.create({
    data: {
      name,
      description,
      startTime,
      endTime,
      tokenName,
      tokenSymbol,
    },
  });
};

const investInICO = async ({ userId, icoId, amount }) => {
  // This function would ideally interact with a smart contract to lock funds and handle the investment logic
  // Placeholder for smart contract interaction
  return 'Investment successful';
};

const fetchICODetails = async (icoId) => {
  return await prisma.iCO.findUnique({
    where: {
      id: icoId,
    },
    include: {
      investments: true, // Include the investors in the response
    },
  });
};

module.exports = { createICO, investInICO, fetchICODetails };